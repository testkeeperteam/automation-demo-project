Feature: Load data with parametric template
	
Scenario Outline: Load
Given A country name  "<name>"
When I load the country information
Then I verify the <status> and "<currency>" are correct


	Examples:
    | name    | currency | status |
    | Poland  |  PLN     | 200    |
    | UK      |  GBP     | 200    |
    | Italy   |  EUR     | 500    |
    

package com.solvedo.training.test.demoproject.loaddata;

import static io.restassured.config.RestAssuredConfig.config;
import static io.restassured.config.XmlConfig.xmlConfig;
import static io.restassured.path.xml.XmlPath.from;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;

import com.solvedo.testing.core.APIUtils;
import com.solvedo.testing.core.DataUtils;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.path.xml.XmlPath;
import io.restassured.path.xml.element.Node;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;


public class LoadStep {
	
	private String countryName;
	private Response response;
	
	@Given("^A country name  \"([^\"]*)\"$")
	public void a_country_name(String name) throws Throwable {
	   countryName = name;
	}

	@When("^I load the country information$")
	public void i_load_the_country_information() throws Throwable {
		//RequestSpecification request = APIUtils.api().buildRequest();
		
		Map<String, String> valueMap = new HashMap<String, String>();
		valueMap.put("countryName", countryName);
		String parametrizeRequest = DataUtils.filter(valueMap,
		        "com/solvedo/training/test/demoproject/loaddata/request.xml");
		
		RequestSpecification request = APIUtils.api().buildRequest();
		response = request
				.config(config().xmlConfig(xmlConfig().namespaceAware(true)))
	    		.contentType("application/xml;charset=utf-8")
	            .body(parametrizeRequest)
	            .post("/services/countries");
	}

	@Then("^I verify the (\\d+) and \"([^\"]*)\" are correct$")
	public void i_verify_the_and_are_correct(int status, String currency) throws Throwable {
		
		ValidatableResponse vr = response.then().statusCode(status);
	  
	    	    
	    String responseAsString = vr.extract().asString();
	    List<String> nodes = from(responseAsString).getList(
                "**.findAll {it.currency.text()=='" + currency + "'}");
	    
	    
	    Node node = null;
		try {
			node = from(responseAsString).getNode("**.findAll {it.currency.text()=='" + currency + "'}");
		} catch (Exception e) {
			Assert.fail("Unable to find nodes containing currency " + currency);
		}
	    Assert.assertNotNull(node);
	    Node currencyNode = node.get("currency");
	    String value = currencyNode.value();
	    Assert.assertEquals(currency, value);
	   
	    
	  

	}

}
